


-record(ctr_realm, {name = undefined,
                    authmethods = [],
                    authmapping = []
                   }).

-record(ctr_subscription, {
          id = undefined,
          realm = undefined,
          uri = undefined,
          match = exact,
          created = undefined,
          subscribers = []
         }).

-record(ctr_publication, {
          id = undefined,
          pub_sess_id = undefined,
          options = undefined,
          details = #{},
          subs = [],
          realm = undefined,
          topic = undefined,
          ts = undefined,
          arguments = undefined,
          argumentskw = undefined
         }).

-record(ctr_registration, {
          id = undefined,
          realm = undefined,
          procedure = undefined,
          match = exact,
          invoke = unknown,
          created = undefined,
          callee_sess_ids = []
         }).


-record(ctr_invocation_caller, {
          id = undefined,
          ts = undefined,
          sess_id = undefined,
	  req_id = undefined,
          realm = undefined,
          pid = undefined
         }).

-record(ctr_invocation_callee, {
	  id = undefined,
          sess_id = undefined,
          pid = undefined
         }).
