-module(ctr_invocation_test).

-include_lib("eunit/include/eunit.hrl").
-include_lib("ct_msg/include/ct_msg.hrl").
-include("ctr_data.hrl").

-define(REALM, <<"realm">>).

-define(TEST_CALL_ID, 1234).
-define(TEST_ERROR_CALL_ID, 12666).
-define(CANCEL_SKIP_CALL_ID, 2345).
-define(CANCEL_SKIP_ERROR_CALL_ID, 23666).
-define(CANCEL_KILL_CALL_ID, 3456).
-define(CANCEL_KILL_ERROR_CALL_ID, 34666).
-define(CANCEL_KILLNOWAIT_CALL_ID, 4567).
-define(CANCEL_KILLNOWAIT_ERROR_CALL_ID, 45666).
-define(TEST_PROGRESS_CALL_ID, 5678).
-define(TEST_PROGRESS_ERROR_CALL_ID, 56666).

-define(TIMEOUT_CALL_ID, 7890).
-define(EXCEPTION_CALL_ID, 666).

-define(CALLEE_SESS_ID, 4242).
-define(CALLER_SESS_ID, 123).

start_stop_test() ->
    {ok, Mock} = start_mock(),
    try
        Msg = ?CALL(1, #{}, <<"test">>),
        Session = #{realm => <<"realm">>, sess_id => ?CALLER_SESS_ID},

        Params = #{
          call_msg => Msg,
          caller_session => Session
         },
        {ok, Pid} = ctr_invocation:start(Params),
        ?assertEqual(true, ct_test_utils:process_running(Pid)),
        ?assertEqual(ready, ctr_invocation:get_state(Pid)),
        ok = ctr_invocation:stop(Pid),
        ?assertEqual(ok, ct_test_utils:wait_for_process_to_die(Pid, 100))
    after
        stop_mock(Mock)
    end,
    ok.

data_test() ->
    {ok, Mock} = start_mock(),
    try
        Msg = ?CALL(1, #{}, <<"test">>),
        Session = #{realm => <<"realm">>, sess_id => ?CALLER_SESS_ID},

        Params = #{
          call_msg => Msg,
          caller_session => Session
         },
        {ok, Pid} = ctr_invocation:start(Params),
        ?assertEqual(true, ct_test_utils:process_running(Pid)),
        ?assertEqual(ready, ctr_invocation:get_state(Pid)),
        Data = ctr_invocation:get_data(Pid),
        ?assertEqual(data, element(1, Data)),
        ok = ctr_invocation:stop(Pid),
        ?assertEqual(ok, ct_test_utils:wait_for_process_to_die(Pid, 100))
    after
        stop_mock(Mock)
    end,
    ok.

garbage_test() ->
    {ok, Mock} = start_mock(),
    try
        Msg = ?CALL(1, #{}, <<"test">>),
        Session = #{realm => <<"realm">>, sess_id => ?CALLER_SESS_ID},

        Params = #{
          call_msg => Msg,
          caller_session => Session
         },
        {ok, Pid} = ctr_invocation:start(Params),
        ?assertEqual(true, ct_test_utils:process_running(Pid)),
        Pid ! dirt,
        gen_statem:cast(Pid, rubbish),
        ?assertEqual(true, ct_test_utils:process_running(Pid)),
        ok = ctr_invocation:stop(Pid),
        ?assertEqual(ok, ct_test_utils:wait_for_process_to_die(Pid, 100))
    after
        stop_mock(Mock)
    end,
    ok.


no_such_procedure_test() ->
    {ok, Mock} = start_mock(),
    try
        {CallerPid, CalleePid, _} = Mock,
        ReqId = ct_utils:gen_global_id(),
        CallerPid ! {req_id, ReqId},
        Msg = ?CALL(ReqId, #{}, <<"not.exist">>),
        Session = #{realm => <<"realm">>, sess_id => ?CALLER_SESS_ID, pid => CallerPid},

        Params = #{
          call_msg => Msg,
          caller_session => Session
         },

        {ok, Pid} = ctr_invocation:start(Params),
        CalleePid ! stop,
        ?assertEqual(ok, ct_test_utils:wait_for_process_to_die(CalleePid, 100)),

        ?assertEqual(true, ct_test_utils:process_running(Pid)),
        ?assertEqual(ready, ctr_invocation:get_state(Pid)),
        ok = ctr_invocation:send_invocation(Pid),
        ?assertEqual(ok, ct_test_utils:wait_for_process_to_die(Pid, 100)),

        CallerPid ! {get_results, self()},
        receive 
           ResultMsgs -> ok
        end,
        ?assertEqual(1, length(ResultMsgs)),
        [{error, call, ResultReqId, _, Error, Args, ArgsKw}] = ResultMsgs,
        ?assertEqual(ReqId, ResultReqId),
        ?assertEqual(no_such_procedure, Error),
        ?assertEqual(undefined, Args),
        ?assertEqual(undefined, ArgsKw),
        ok
    after
        stop_mock(Mock)
    end,
    ok.

internal_call_test() ->
    {ok, Mock} = start_mock(),
    try
        {CallerPid, CalleePid, _} = Mock,
        ReqId = ct_utils:gen_global_id(),
        CallerPid ! {req_id, ReqId},
        Msg = ?CALL(ReqId, #{}, <<"wamp.session.count">>),
        Session = #{realm => <<"realm">>, sess_id => ?CALLER_SESS_ID, pid => CallerPid},

        Params = #{
          call_msg => Msg,
          caller_session => Session
         },

        {ok, Pid} = ctr_invocation:start(Params),
        CalleePid ! stop,
        ?assertEqual(ok, ct_test_utils:wait_for_process_to_die(CalleePid, 100)),

        ?assertEqual(true, ct_test_utils:process_running(Pid)),
        ?assertEqual(ready, ctr_invocation:get_state(Pid)),
        ok = ctr_invocation:send_invocation(Pid),
        ?assertEqual(ok, ct_test_utils:wait_for_process_to_die(Pid, 100)),

        CallerPid ! {get_results, self()},
        receive 
           ResultMsgs -> ok
        end,
        ?assertEqual(1, length(ResultMsgs)),
        [{result, ResultReqId, _Details, Args, ArgsKw}] = ResultMsgs,
        ?assertEqual(ReqId, ResultReqId),
        ?assertEqual([3], Args),
        ?assertEqual(undefined, ArgsKw),
        ok
    after
        stop_mock(Mock)
    end,
    ok.


external_call_result_test() ->
    {ok, Mock} = start_mock(),
    try
        {CallerPid, CalleePid, _} = Mock,
        ReqId = ct_utils:gen_global_id(),
        CallerPid ! {req_id, ReqId},
        Msg = ?CALL(ReqId, #{}, <<"test">>),
        Session = #{realm => <<"realm">>, sess_id => ?CALLER_SESS_ID, pid => CallerPid},

        Params = #{
          call_msg => Msg,
          caller_session => Session
         },

        {ok, Pid} = ctr_invocation:start(Params),
        CalleePid ! {invocation_pid, Pid},

        ?assertEqual(true, ct_test_utils:process_running(Pid)),
        ?assertEqual(ready, ctr_invocation:get_state(Pid)),
        ok = ctr_invocation:send_invocation(Pid),
        ?assertEqual(wait_for_result, ctr_invocation:get_state(Pid)),
        ?assertEqual(ok, ct_test_utils:wait_for_process_to_die(Pid, 100)),

        CallerPid ! {get_results, self()},
        receive 
           ResultMsgs -> ok
        end,
        ?assertEqual(1, length(ResultMsgs)),
        [{result, ResultReqId, _Details, Args, ArgsKw}] = ResultMsgs,
        ?assertEqual(ReqId, ResultReqId),
        ?assertEqual([works], Args),
        ?assertEqual(undefined, ArgsKw),
        ok
    after
        stop_mock(Mock)
    end,
    ok.


external_call_error_test() ->
    {ok, Mock} = start_mock(),
    try
        {CallerPid, CalleePid, _} = Mock,
        ReqId = ct_utils:gen_global_id(),
        CallerPid ! {req_id, ReqId},
        Msg = ?CALL(ReqId, #{}, <<"test.error">>),
        Session = #{realm => <<"realm">>, sess_id => ?CALLER_SESS_ID, pid => CallerPid},

        Params = #{
          call_msg => Msg,
          caller_session => Session
         },

        {ok, Pid} = ctr_invocation:start(Params),
        CalleePid ! {invocation_pid, Pid},

        ?assertEqual(true, ct_test_utils:process_running(Pid)),
        ?assertEqual(ready, ctr_invocation:get_state(Pid)),
        ok = ctr_invocation:send_invocation(Pid),
        ?assertEqual(wait_for_result, ctr_invocation:get_state(Pid)),
        ?assertEqual(ok, ct_test_utils:wait_for_process_to_die(Pid, 100)),

        CallerPid ! {get_results, self()},
        receive 
           ResultMsgs -> ok
        end,
        ?assertEqual(1, length(ResultMsgs)),
        [{error, call, ResultReqId, _, Error, Args, ArgsKw}] = ResultMsgs,
        ?assertEqual(ReqId, ResultReqId),
        ?assertEqual(<<"call.error">>, Error),
        ?assertEqual(undefined, Args),
        ?assertEqual(undefined, ArgsKw),
        ok
    after
        stop_mock(Mock)
    end,
    ok.


external_call_cancel_skip_result_test() ->
    {ok, Mock} = start_mock(),
    try
        {CallerPid, CalleePid, _} = Mock,
        ReqId = ct_utils:gen_global_id(),
        CallerPid ! {req_id, ReqId},
        Msg = ?CALL(ReqId, #{}, <<"test.cancel.skip">>),
        Session = #{realm => <<"realm">>, sess_id => ?CALLER_SESS_ID, pid => CallerPid},

        Params = #{
          call_msg => Msg,
          caller_session => Session
         },

        {ok, Pid} = ctr_invocation:start(Params),
        CalleePid ! {invocation_pid, Pid},

        ?assertEqual(true, ct_test_utils:process_running(Pid)),
        ?assertEqual(ready, ctr_invocation:get_state(Pid)),
        ok = ctr_invocation:send_invocation(Pid),
        ?assertEqual(wait_for_result, ctr_invocation:get_state(Pid)),
        ctr_invocation:handle_message(?CANCEL(ReqId, #{}), ?CALLER_SESS_ID, Pid),
        ?assertEqual(cancelled, ctr_invocation:get_state(Pid)),

        CallerPid ! {get_results, self()},
        receive 
           ResultMsgs -> ok
        end,
        ?assertEqual(1, length(ResultMsgs)),
        [{error, call, ResultReqId, _, Error, Args, ArgsKw}] = ResultMsgs,
        ?assertEqual(ReqId, ResultReqId),
        ?assertEqual(cancelled, Error),
        ?assertEqual(undefined, Args),
        ?assertEqual(undefined, ArgsKw),
        ok
    after
        stop_mock(Mock)
    end,
    ok.

external_call_cancel_skip_error_test() ->
    {ok, Mock} = start_mock(),
    try
        {CallerPid, CalleePid, _} = Mock,
        ReqId = ct_utils:gen_global_id(),
        CallerPid ! {req_id, ReqId},
        Msg = ?CALL(ReqId, #{}, <<"test.cancel.skip.error">>),
        Session = #{realm => <<"realm">>, sess_id => ?CALLER_SESS_ID, pid => CallerPid},

        Params = #{
          call_msg => Msg,
          caller_session => Session
         },

        {ok, Pid} = ctr_invocation:start(Params),
        CalleePid ! {invocation_pid, Pid},

        ?assertEqual(true, ct_test_utils:process_running(Pid)),
        ?assertEqual(ready, ctr_invocation:get_state(Pid)),
        ok = ctr_invocation:send_invocation(Pid),
        ?assertEqual(wait_for_result, ctr_invocation:get_state(Pid)),
        ctr_invocation:handle_message(?CANCEL(ReqId, #{}), ?CALLER_SESS_ID, Pid),
        ?assertEqual(cancelled, ctr_invocation:get_state(Pid)),

        CallerPid ! {get_results, self()},
        receive 
           ResultMsgs -> ok
        end,
        ?assertEqual(1, length(ResultMsgs)),
        [{error, call, ResultReqId, _, Error, Args, ArgsKw}] = ResultMsgs,
        ?assertEqual(ReqId, ResultReqId),
        ?assertEqual(cancelled, Error),
        ?assertEqual(undefined, Args),
        ?assertEqual(undefined, ArgsKw),
        ok
    after
        stop_mock(Mock)
    end,
    ok.

external_call_cancel_kill_result_test() ->
    {ok, Mock} = start_mock(),
    try
        {CallerPid, CalleePid, _} = Mock,
        ReqId = ct_utils:gen_global_id(),
        CallerPid ! {req_id, ReqId},
        Msg = ?CALL(ReqId, #{}, <<"test.cancel.kill">>),
        Session = #{realm => <<"realm">>, sess_id => ?CALLER_SESS_ID, pid => CallerPid},

        Params = #{
          call_msg => Msg,
          caller_session => Session
         },

        {ok, Pid} = ctr_invocation:start(Params),
        CalleePid ! {invocation_pid, Pid},

        ?assertEqual(true, ct_test_utils:process_running(Pid)),
        ?assertEqual(ready, ctr_invocation:get_state(Pid)),
        ok = ctr_invocation:send_invocation(Pid),
        ?assertEqual(wait_for_result, ctr_invocation:get_state(Pid)),
        ctr_invocation:handle_message(?CANCEL(ReqId, #{mode => kill}), ?CALLER_SESS_ID, Pid),
        ?assertEqual(wait_for_result, ctr_invocation:get_state(Pid)),

	CalleePid ! stop,
    	ok = ct_test_utils:wait_for_process_to_die(CalleePid, 20),

        CallerPid ! {get_results, self()},
        receive 
           ResultMsgs -> ok
        end,
        ?assertEqual(1, length(ResultMsgs)),
        [{error, call, ResultReqId, _, Error, Args, ArgsKw}] = ResultMsgs,
        ?assertEqual(ReqId, ResultReqId),
        ?assertEqual(<<"cancelled.interrupt">>, Error),
        ?assertEqual(undefined, Args),
        ?assertEqual(undefined, ArgsKw),
        ok
    after
        stop_mock(Mock)
    end,
    ok.

external_call_cancel_kill_error_test() ->
    {ok, Mock} = start_mock(),
    try
        {CallerPid, CalleePid, _} = Mock,
        ReqId = ct_utils:gen_global_id(),
        CallerPid ! {req_id, ReqId},
        Msg = ?CALL(ReqId, #{}, <<"test.cancel.kill.error">>),
        Session = #{realm => <<"realm">>, sess_id => ?CALLER_SESS_ID, pid => CallerPid},

        Params = #{
          call_msg => Msg,
          caller_session => Session
         },

        {ok, Pid} = ctr_invocation:start(Params),
        CalleePid ! {invocation_pid, Pid},

        ?assertEqual(true, ct_test_utils:process_running(Pid)),
        ?assertEqual(ready, ctr_invocation:get_state(Pid)),
        ok = ctr_invocation:send_invocation(Pid),
        ?assertEqual(wait_for_result, ctr_invocation:get_state(Pid)),
        ctr_invocation:handle_message(?CANCEL(ReqId, #{mode => kill}), ?CALLER_SESS_ID, Pid),
        ?assertEqual(wait_for_result, ctr_invocation:get_state(Pid)),

	CalleePid ! stop,
    	ok = ct_test_utils:wait_for_process_to_die(CalleePid, 20),

        CallerPid ! {get_results, self()},
        receive 
           ResultMsgs -> ok
        end,
        ?assertEqual(1, length(ResultMsgs)),
        [{error, call, ResultReqId, _, Error, Args, ArgsKw}] = ResultMsgs,
        ?assertEqual(ReqId, ResultReqId),
        ?assertEqual(<<"cancelled.interrupt">>, Error),
        ?assertEqual(undefined, Args),
        ?assertEqual(undefined, ArgsKw),
        ok
    after
        stop_mock(Mock)
    end,
    ok.

external_call_cancel_killnowait_result_test() ->
    {ok, Mock} = start_mock(),
    try
        {CallerPid, CalleePid, _} = Mock,
        ReqId = ct_utils:gen_global_id(),
        CallerPid ! {req_id, ReqId},
        Msg = ?CALL(ReqId, #{}, <<"test.cancel.killnowait">>),
        Session = #{realm => <<"realm">>, sess_id => ?CALLER_SESS_ID, pid => CallerPid},

        Params = #{
          call_msg => Msg,
          caller_session => Session
         },

        {ok, Pid} = ctr_invocation:start(Params),
        CalleePid ! {invocation_pid, Pid},

        ?assertEqual(true, ct_test_utils:process_running(Pid)),
        ?assertEqual(ready, ctr_invocation:get_state(Pid)),
        ok = ctr_invocation:send_invocation(Pid),
        ?assertEqual(wait_for_result, ctr_invocation:get_state(Pid)),
        ctr_invocation:handle_message(?CANCEL(ReqId, #{mode => killnowait}), ?CALLER_SESS_ID, Pid),
        ?assertEqual(cancelled, ctr_invocation:get_state(Pid)),

        CallerPid ! {get_results, self()},
        receive 
           ResultMsgs -> ok
        end,
        ?assertEqual(1, length(ResultMsgs)),
        [{error, call, ResultReqId, _, Error, Args, ArgsKw}] = ResultMsgs,
        ?assertEqual(ReqId, ResultReqId),
        ?assertEqual(cancelled, Error),
        ?assertEqual(undefined, Args),
        ?assertEqual(undefined, ArgsKw),
        ok
    after
        stop_mock(Mock)
    end,
    ok.

external_call_cancel_killnowait_error_test() ->
    {ok, Mock} = start_mock(),
    try
        {CallerPid, CalleePid, _} = Mock,
        ReqId = ct_utils:gen_global_id(),
        CallerPid ! {req_id, ReqId},
        Msg = ?CALL(ReqId, #{}, <<"test.cancel.killnowait.error">>),
        Session = #{realm => <<"realm">>, sess_id => ?CALLER_SESS_ID, pid => CallerPid},

        Params = #{
          call_msg => Msg,
          caller_session => Session
         },

        {ok, Pid} = ctr_invocation:start(Params),
        CalleePid ! {invocation_pid, Pid},

        ?assertEqual(true, ct_test_utils:process_running(Pid)),
        ?assertEqual(ready, ctr_invocation:get_state(Pid)),
        ok = ctr_invocation:send_invocation(Pid),
        ?assertEqual(wait_for_result, ctr_invocation:get_state(Pid)),
        ctr_invocation:handle_message(?CANCEL(ReqId, #{mode => killnowait}), ?CALLER_SESS_ID, Pid),
        ?assertEqual(cancelled, ctr_invocation:get_state(Pid)),

        CallerPid ! {get_results, self()},
        receive 
           ResultMsgs -> ok
        end,
        ?assertEqual(1, length(ResultMsgs)),
        [{error, call, ResultReqId, _, Error, Args, ArgsKw}] = ResultMsgs,
        ?assertEqual(ReqId, ResultReqId),
        ?assertEqual(cancelled, Error),
        ?assertEqual(undefined, Args),
        ?assertEqual(undefined, ArgsKw),
        ok
    after
        stop_mock(Mock)
    end,
    ok.

external_call_progress_test() ->
    {ok, Mock} = start_mock(),
    try
        {CallerPid, CalleePid, _} = Mock,
        ReqId = ct_utils:gen_global_id(),
        CallerPid ! {req_id, ReqId},
        Msg = ?CALL(ReqId, #{receive_progress => true}, <<"test.progress">>),
        Session = #{realm => <<"realm">>, sess_id => ?CALLER_SESS_ID, pid => CallerPid},

        Params = #{
          call_msg => Msg,
          caller_session => Session
         },

        {ok, Pid} = ctr_invocation:start(Params),
        CalleePid ! {invocation_pid, Pid},

        ?assertEqual(true, ct_test_utils:process_running(Pid)),
        ?assertEqual(ready, ctr_invocation:get_state(Pid)),
        ok = ctr_invocation:send_invocation(Pid),
        ?assertEqual(wait_for_result, ctr_invocation:get_state(Pid)),

	CalleePid ! stop,
        ct_test_utils:wait_for_process_to_die(CalleePid, 100),

        CallerPid ! {get_results, self()},
        receive 
           ResultMsgs -> ok
        end,
        ?assertEqual(3, length(ResultMsgs)),
        ct_test_utils:wait_for_process_to_die(Pid, 100),
        ok
    after
        stop_mock(Mock)
    end,
    ok.

external_call_progress_error_test() ->
    {ok, Mock} = start_mock(),
    try
        {CallerPid, CalleePid, _} = Mock,
        ReqId = ct_utils:gen_global_id(),
        CallerPid ! {req_id, ReqId},
        Msg = ?CALL(ReqId, #{receive_progress => true}, <<"test.progress.error">>),
        Session = #{realm => <<"realm">>, sess_id => ?CALLER_SESS_ID, pid => CallerPid},

        Params = #{
          call_msg => Msg,
          caller_session => Session
         },

        {ok, Pid} = ctr_invocation:start(Params),
        CalleePid ! {invocation_pid, Pid},

        ?assertEqual(true, ct_test_utils:process_running(Pid)),
        ?assertEqual(ready, ctr_invocation:get_state(Pid)),
        ok = ctr_invocation:send_invocation(Pid),
        ?assertEqual(wait_for_result, ctr_invocation:get_state(Pid)),

	CalleePid ! stop,
        ct_test_utils:wait_for_process_to_die(CalleePid, 100),

        CallerPid ! {get_results, self()},
        receive 
           ResultMsgs -> ok
        end,
        ?assertEqual(2, length(ResultMsgs)),
        ct_test_utils:wait_for_process_to_die(Pid, 100),
        ok
    after
        stop_mock(Mock)
    end,
    ok.

external_call_timeout_test() ->
    {ok, Mock} = start_mock(),
    try
        {CallerPid, CalleePid, _} = Mock,
        ReqId = ct_utils:gen_global_id(),
        CallerPid ! {req_id, ReqId},
        Msg = ?CALL(ReqId, #{timeout => 50}, <<"test">>),
        Session = #{realm => <<"realm">>, sess_id => ?CALLER_SESS_ID, pid => CallerPid},

        Params = #{
          call_msg => Msg,
          caller_session => Session
         },

        {ok, Pid} = ctr_invocation:start(Params),
        CalleePid ! {invocation_pid, Pid},

        ?assertEqual(true, ct_test_utils:process_running(Pid)),
        ?assertEqual(ready, ctr_invocation:get_state(Pid)),
        ok = ctr_invocation:send_invocation(Pid),
        ?assertEqual(wait_for_result, ctr_invocation:get_state(Pid)),

        IsCancelled = fun() ->
                                cancelled == ctr_invocation:get_state(Pid)
                      end,
        ct_test_utils:wait_for_true(IsCancelled, 100),

        CallerPid ! {get_results, self()},
        receive 
           ResultMsgs -> ok
        end,
        ?assertEqual(1, length(ResultMsgs)),
        [{error, call, ResultReqId, _, Error, Args, ArgsKw}] = ResultMsgs,
        ?assertEqual(ReqId, ResultReqId),
        ?assertEqual(cancelled, Error),
        ?assertEqual(undefined, Args),
        ?assertEqual(undefined, ArgsKw),
        ok
    after
        stop_mock(Mock)
    end,
    ok.

external_call_exception_test() ->
    {ok, Mock} = start_mock(),
    try
        {CallerPid, CalleePid, _} = Mock,
        ReqId = ct_utils:gen_global_id(),
        CallerPid ! {req_id, ReqId},
        Msg = ?CALL(ReqId, #{}, <<"exception">>),
        Session = #{realm => <<"realm">>, sess_id => ?CALLER_SESS_ID, pid => CallerPid},

        Params = #{
          call_msg => Msg,
          caller_session => Session
         },

        {ok, Pid} = ctr_invocation:start(Params),
        CalleePid ! {invocation_pid, Pid},

        ?assertEqual(true, ct_test_utils:process_running(Pid)),
        ?assertEqual(ready, ctr_invocation:get_state(Pid)),
        ok = ctr_invocation:send_invocation(Pid),
        ?assertEqual(wait_for_result, ctr_invocation:get_state(Pid)),

        ct_test_utils:wait_for_process_to_die(Pid, 100),

        CallerPid ! {get_results, self()},
        receive 
           ResultMsgs -> ok
        end,
        ?assertEqual(1, length(ResultMsgs)),
        [{error, call, ResultReqId, _, Error, Args, ArgsKw}] = ResultMsgs,
        ?assertEqual(ReqId, ResultReqId),
        ?assertEqual(<<"org.cargotube.error.internal">>, Error),
        ?assertEqual(undefined, Args),
        ?assertEqual(undefined, ArgsKw),
        ok
    after
        stop_mock(Mock)
    end,
    ok.

start_mock() ->
    CallerPid = spawn(fun() -> mock_caller(undefined, []) end ),
    CalleePid = spawn(fun() ->  mock_callee(undefined) end),

    Modules = [cta_session, ctr_data, ctr_registration, ctr_callee_session],

    GetRealm = fun(#{realm := Realm}) -> Realm end,
    GetSessId = fun(#{sess_id := SessId}) -> SessId end,
    SessIsDisclose = fun(Sess) -> maps:get(disclose, Sess, false) end,
    SessLookupById = fun(_Id) -> {ok, #{ pid => CalleePid, realm => <<"realm">> }}  end,
    SessGetPeer = fun(#{pid := Pid}) -> Pid  end,
    SessGetDetails = fun(Map) -> maps:get(details, Map, #{roles => #{callee => 
								     #{features => #{
									 progressive_call_results => true,
									 call_canceling => true
									}}}})  end,

    AddInvocation = fun(Invoc) -> {ok, Invoc#ctr_invocation_caller{id = 42}} end,
    AddInvocationCallee = fun(_Invoc) -> ok end,
    RemoveInvocation = fun(_Realm, _InvocId) -> ok end,

    MatchRegistration = fun(Procedure, Realm) -> 
                                case {Procedure, Realm} of 
                                        {<<"test">>, _} -> {ok, proc_test};
                                        {<<"test.error">>, _} -> {ok, proc_test_error};
                                        {<<"test.cancel.skip">>, _} -> {ok, proc_cancel_skip_test};
                                        {<<"test.cancel.skip.error">>, _} -> {ok, proc_cancel_skip_error_test};
                                        {<<"test.cancel.kill">>, _} -> {ok, proc_cancel_kill_test};
                                        {<<"test.cancel.kill.error">>, _} -> {ok, proc_cancel_kill_error_test};
                                        {<<"test.cancel.killnowait">>, _} -> {ok, proc_cancel_killnowait_test};
                                        {<<"test.cancel.killnowait.error">>, _} -> {ok, proc_cancel_killnowait_error_test};
                                        {<<"test.progress">>, _} -> {ok, proc_progress_test};
                                        {<<"test.progress.error">>, _} -> {ok, proc_progress_error_test};
                                        {<<"exception">>, _} -> {ok, proc_exception_test};
                                        _ -> {error, not_found}
                                end
                        end,
    RegistrationGetId = fun(Registration) -> 
                                case Registration of 
                                        proc_test -> ?TEST_CALL_ID;
                                        proc_test_error -> ?TEST_ERROR_CALL_ID;
                                        proc_cancel_skip_test -> ?CANCEL_SKIP_CALL_ID;
                                        proc_cancel_skip_error_test -> ?CANCEL_SKIP_ERROR_CALL_ID;
                                        proc_cancel_kill_test -> ?CANCEL_KILL_CALL_ID;
                                        proc_cancel_kill_error_test -> ?CANCEL_KILL_ERROR_CALL_ID;
                                        proc_cancel_killnowait_test -> ?CANCEL_KILLNOWAIT_CALL_ID;
                                        proc_cancel_killnowait_error_test -> ?CANCEL_KILLNOWAIT_ERROR_CALL_ID;
                                        proc_progress_test -> ?TEST_PROGRESS_CALL_ID;
                                        proc_progress_error_test -> ?TEST_PROGRESS_ERROR_CALL_ID;
                                        proc_exception_test -> ?EXCEPTION_CALL_ID;
                                        _ -> 1 
                                end
                        end,
    RegistrationGetCallees = fun(_Registration) -> [?CALLEE_SESS_ID] end,

    CalleeSessCount = fun(_, _, _) -> {[3], undefined} end,

    ct_test_utils:meck_new(Modules),
    ok = meck:expect(cta_session, get_realm, GetRealm),
    ok = meck:expect(cta_session, get_id, GetSessId),
    ok = meck:expect(cta_session, get_details, SessGetDetails),
    ok = meck:expect(cta_session, is_disclose_caller, SessIsDisclose),
    ok = meck:expect(cta_session, lookup_by_id, SessLookupById),
    ok = meck:expect(cta_session, get_peer, SessGetPeer),

    ok = meck:expect(ctr_data, add_invocation, AddInvocation),
    ok = meck:expect(ctr_data, add_invocation_callee, AddInvocationCallee),
    ok = meck:expect(ctr_data, remove_invocation, RemoveInvocation),

    ok = meck:expect(ctr_registration, match, MatchRegistration),
    ok = meck:expect(ctr_registration, get_id, RegistrationGetId),
    ok = meck:expect(ctr_registration, get_callees, RegistrationGetCallees),

    ok = meck:expect(ctr_callee_session, count, CalleeSessCount),



    {ok, {CallerPid, CalleePid, Modules}}.



mock_caller(ReqId, Results) ->
    receive 
        stop -> ok;
        {req_id, NewReqId} ->
            mock_caller(NewReqId, Results);
        {get_results, Pid} ->
            Pid ! Results,
            mock_caller(ReqId, Results);
        Msg -> 
	    io:format("caller ~p: received ~p~n", [self(), Msg]),
            handle_caller_msg(Msg, ReqId, Results)
    end.

handle_caller_msg({to_peer, {result, ReqId, #{}, _, _ } = Result}, ReqId, Results) ->
    mock_caller(ReqId, [Result | Results]);
handle_caller_msg({to_peer, {error, call, ReqId, #{}, _, _, _ } = Result}, ReqId, Results) ->
    mock_caller(ReqId, [Result | Results]);
handle_caller_msg(Msg, ReqId, Results) ->
    io:format("caller ~p: received ~p", [self(), Msg]),
    mock_caller(ReqId, Results).


mock_callee(Pid) ->
    receive 
        stop -> ok;
        {invocation_pid, NewPid} -> 
            mock_callee(NewPid);
        Msg -> 
    	    io:format("callee ~p: received ~p~n", [self(), Msg]),
            handle_callee_msg(Msg, Pid),
            mock_callee(Pid)
    end.


handle_callee_msg({to_peer, {invocation, ReqId, ?TEST_CALL_ID, #{}, undefined, undefined }}, Pid) ->
    timer:sleep(100),
    Msg = ?YIELD(ReqId, #{},[works]),
    ctr_invocation:handle_message(Msg, ?CALLEE_SESS_ID, Pid),
    ok;
handle_callee_msg({to_peer, {invocation, ReqId, ?TEST_ERROR_CALL_ID, #{}, undefined, undefined }}, Pid) ->
    timer:sleep(100),
    Msg = ?ERROR(invocation, ReqId, #{}, <<"call.error">>),
    ctr_invocation:handle_message(Msg, ?CALLEE_SESS_ID, Pid),
    ok;
handle_callee_msg({to_peer, {invocation, ReqId, ?CANCEL_SKIP_CALL_ID, #{}, undefined, undefined }}, Pid) ->
    % this will be cancelled
    timer:sleep(100),
    Msg = ?YIELD(ReqId, #{},[cancelled]),
    ctr_invocation:handle_message(Msg, ?CALLEE_SESS_ID, Pid),
    ok;
handle_callee_msg({to_peer, {invocation, ReqId, ?CANCEL_SKIP_ERROR_CALL_ID, #{}, undefined, undefined }}, Pid) ->
    % this will be cancelled
    timer:sleep(100),
    Msg = ?ERROR(invocation, ReqId, #{}, <<"skip.error">>),
    ctr_invocation:handle_message(Msg, ?CALLEE_SESS_ID, Pid),
    ok;
handle_callee_msg({to_peer, {invocation, _ReqId, ?CANCEL_KILL_CALL_ID, #{}, undefined, undefined }}, _Pid) ->
    % this will be cancelled
    ok;
handle_callee_msg({to_peer, {invocation, _ReqId, ?CANCEL_KILL_ERROR_CALL_ID, #{}, undefined, undefined }}, _Pid) ->
    % this will be cancelled
    ok;
handle_callee_msg({to_peer, {interrupt, ReqId, _}}, Pid) ->
    %% cancellintg a call
    timer:sleep(100),
    Msg = ?ERROR(invocation, ReqId, #{}, <<"cancelled.interrupt">>),
    io:format("sending: ~p~n",[Msg]), 
    ctr_invocation:handle_message(Msg, ?CALLEE_SESS_ID, Pid),
    ok;
handle_callee_msg({to_peer, {invocation, ReqId, ?CANCEL_KILLNOWAIT_CALL_ID, #{}, undefined, undefined }}, Pid) ->
    % this will be cancelled
    timer:sleep(100),
    Msg = ?YIELD(ReqId, #{},[cancelled]),
    ctr_invocation:handle_message(Msg, ?CALLEE_SESS_ID, Pid),
    ok;
handle_callee_msg({to_peer, {invocation, ReqId, ?CANCEL_KILLNOWAIT_ERROR_CALL_ID, #{}, undefined, undefined }}, Pid) ->
    % this will be cancelled
    timer:sleep(100),
    Msg = ?ERROR(invocation, ReqId, #{}, <<"skip.error">>),
    ctr_invocation:handle_message(Msg, ?CALLEE_SESS_ID, Pid),
    ok;
handle_callee_msg({to_peer, {invocation, ReqId, ?EXCEPTION_CALL_ID, #{}, undefined, undefined }}, Pid) ->
    timer:sleep(100),
    Msg = {yield, ReqId}, 
    ctr_invocation:handle_message(Msg, ?CALLEE_SESS_ID, Pid),
    ok;
handle_callee_msg({to_peer, {invocation, ReqId, ?TEST_PROGRESS_CALL_ID, #{receive_progress := true}, undefined, undefined }}, Pid) ->
    io:format("callee ~p: sending part 1/3~n",[self()]),
    Msg1 = ?YIELD(ReqId, #{progress => true},[a]),
    ctr_invocation:handle_message(Msg1, ?CALLEE_SESS_ID, Pid),
    timer:sleep(20),
    io:format("callee ~p: sending part 2/3~n",[self()]),
    Msg2 = ?YIELD(ReqId, #{progress => true},[b]),
    ctr_invocation:handle_message(Msg2, ?CALLEE_SESS_ID, Pid),
    timer:sleep(20),
    io:format("callee ~p: sending part 3/3~n",[self()]),
    Msg3 = ?YIELD(ReqId, #{progress => false},[c]),
    ctr_invocation:handle_message(Msg3, ?CALLEE_SESS_ID, Pid),
    ok;
handle_callee_msg({to_peer, {invocation, ReqId, ?TEST_PROGRESS_ERROR_CALL_ID, #{receive_progress := true}, undefined, undefined }}, Pid) ->
    io:format("callee ~p: sending part 1/2~n",[self()]),
    Msg1 = ?YIELD(ReqId, #{progress => true},[a]),
    ctr_invocation:handle_message(Msg1, ?CALLEE_SESS_ID, Pid),
    timer:sleep(20),
    io:format("callee ~p: sending part 2/2~n",[self()]),
    Msg2 = ?ERROR(invocation, ReqId, #{}, <<"progress.error">>),
    ctr_invocation:handle_message(Msg2, ?CALLEE_SESS_ID, Pid),
    ok;
handle_callee_msg(_Msg, _Pid) ->
    io:format("callee ~p: message skipped", [self()]),
    ok.

stop_mock({CallerPid, CalleePid, Modules}) ->
    CallerPid ! stop,
    CalleePid ! stop,
    ok = ct_test_utils:wait_for_process_to_die(CallerPid, 100),
    ok = ct_test_utils:wait_for_process_to_die(CalleePid, 100),
    ct_test_utils:meck_done(Modules),
    ok.
