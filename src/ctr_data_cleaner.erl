-module(ctr_data_cleaner).

-behaviour(gen_server).


%%
-export([start_link/0]).
-export([start/1]).
-export([stop/0]).


%% gen_server.
-export([init/1]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([terminate/2]).
-export([code_change/3]).


start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, no_param, []).

start(Config) ->
    gen_server:call(?MODULE, {start, Config}).

stop() ->
    gen_server:call(?MODULE, stop).

-record(state, {
          enabled = false,
          max_age = 21600, % 6 hours
          interval = 300
         }).

init(no_param) ->
    {ok, #state{}}.

handle_call({start, Config}, _From, #state{max_age=MaxAge0,
                                           interval=Interval0} = State) ->
    MaxAge = maps:get(max_age, Config, MaxAge0),
    Interval = maps:get(interval, Config, Interval0),
    lager:info("data cleaner with interval of ~p seconds [MaxAge = ~p sec]",
                [Interval, MaxAge]),
    {reply, ok, State#state{enabled = true, max_age= MaxAge,
                            interval=Interval}, 100};
handle_call(stop, _From, State) ->
    {reply, ok, State#state{enabled = false}}.

handle_cast(Msg, State) ->
    lager:warning("unexpected message: ~p", [Msg]),
    {noreply, State, 15000}.

handle_info(timeout, #state{enabled = true, interval=Interval} = State) ->
    ok = clean_db(State),
    {noreply, State, Interval*1000}.



clean_db(#state{max_age=MaxAge}) ->
    lager:debug("cleaning data"),
    ctr_data:clean_publications(MaxAge),
    ok.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.
