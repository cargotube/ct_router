-module(ctr_invocation).
-behaviour(gen_statem).

-include_lib("ct_msg/include/ct_msg.hrl").
-include("ctr_data.hrl").
-define(DELETE, application:get_env(ct_router, delete_invocation, true)).

%%
-export([new/2]).
-export([start_link/1]).
-export([start/1]).
-export([send_invocation/1]).
-export([handle_message/3]).
-export([get_state/1]).
-export([get_data/1]).
-export([session_terminated/2]).
-export([stop/1]).


%%gen_statem
-export([callback_mode/0]).
-export([init/1]).
-export([handle_event/4]).
-export([terminate/3]).
-export([code_change/4]).


new(CallMsg, CallerSession) ->
    Params = #{
        call_msg => CallMsg,
        caller_session => CallerSession
     },
   ctr_invocation_sup:new_invocation(Params).

start_link(InvocationData) ->
    gen_statem:start_link(?MODULE, InvocationData, []).

start(InvocationData) ->
    gen_statem:start(?MODULE, InvocationData, []).

send_invocation(Pid) when is_pid(Pid) ->
    gen_statem:call(Pid, send_invocation).

handle_message(Msg, SessionId, Pid) when is_pid(Pid) ->
    gen_statem:call(Pid, {handle_message, Msg, SessionId}).

stop(Pid) ->
    gen_statem:cast(Pid, stop).

session_terminated(SessId, Pid) ->
    gen_statem:cast(Pid, {session_terminated, SessId}).


%% helper functions for debugging / testing
get_state(Pid) ->
    gen_statem:call(Pid, get_state).

get_data(Pid) ->
    gen_statem:call(Pid, get_data).

%% use the handle event function
callback_mode() -> [ handle_event_function, state_enter ].


-record(data, {
          id = undefined,
          caller_sess = undefined,
          caller_req_id = undefined,
          call_msg = undefined,
          options = #{},
          procedure = <<>>,
          registration = undefined,
          callees = [],
          callees_waiting_for =[],
          realm = undefined
         }).


init( #{ call_msg := Msg, caller_session := CallerSess } ) ->
    {call, ReqId, _, Procedure, _, _} = Msg,
    Realm = cta_session:get_realm(CallerSess),
    Data = #data{
            caller_sess = CallerSess,
            call_msg = Msg,
            caller_req_id = ReqId,
            procedure = Procedure,
            realm = Realm
            },
    {ok, ready, Data}.

%% handle helper calls for debugging / testing
handle_event(cast, stop, _State, Data) ->
    {stop, normal, Data};
handle_event({call, From}, get_state, State, Data) ->
    {next_state, State, Data, {reply, From, State}};
handle_event({call, From}, get_data, State, Data) ->
    {next_state, State, Data, {reply, From, Data}};

%% setup the invocation by looking up the registration of the procedure
handle_event(enter, _, ready,
  #data{caller_req_id = ReqId, procedure=Procedure,
        caller_sess = CSess} = Data) ->
    {ok, Invoc} = ctrd_invocation:new(self(), ReqId, CSess),
    #ctr_invocation_caller{id = InvocId} = Invoc,
    Internal = ctr_callee:is_procedure(Procedure),
    Registration = match_registration(Internal, Procedure, CSess),
    {next_state, ready, Data#data{id = InvocId, registration=Registration}};


%% procedure does not exist
handle_event({call, From}, send_invocation, ready,
             #data{registration = {error, not_found}, call_msg = Msg,
                   caller_sess = CSess} = Data) ->
    {ok, RequestId} = ct_msg:get_request_id(Msg),
    Error = ?ERROR(call, RequestId, #{}, no_such_procedure),
    ok = ct_router:to_session(CSess, Error),
    {next_state, done, Data, {reply, From, ok}};

%% an internal call
handle_event({call, From}, send_invocation, ready,
             #data{registration = {ok, system}, call_msg=Msg,
                   caller_sess = CSess} = Data) ->
    Response = ctr_callee:handle_call(Msg, CSess),
    ok = ct_router:to_session(CSess, Response),
    {next_state, done, Data, {reply, From, ok}};

%% an external call
handle_event({call, From}, send_invocation, ready,
             #data{id=InvocId, registration = {ok, Registration},
                   call_msg = Msg, caller_sess = CallerSess} = Data) ->
    {call, _ReqId, Options, _Procedure, Args, ArgsKw} = Msg,
    RegId = ctr_registration:get_id(Registration),
    CalleeIds = ctr_registration:get_callees(Registration),
    ok = ctrd_invocation:add_callees(self(), InvocId, CalleeIds),

    DiscloseSession = cta_session:is_disclose_caller(CallerSess),
    DiscloseOption = maps:get(disclose_me, Options, false),
    ProgressOption = maps:get(receive_progress, Options, false),
    Disclose = DiscloseSession or DiscloseOption,


    TimeoutOption = maps:get(timeout, Options, 0),

    ConvOpts = #{
      disclose => Disclose,
      progress => ProgressOption,
      timeout => TimeoutOption
     },

    NewData = Data#data{options = ConvOpts, callees = CalleeIds,
                        callees_waiting_for = CalleeIds},
    send_invocation(InvocId, RegId, CalleeIds, Args, ArgsKw, NewData),
    Actions = [ {reply, From, ok} | maybe_timeout_action(TimeoutOption)],
    {next_state, wait_for_result, NewData, Actions };


%% handle events that either comes from the caller or the callee after
%% sending the invocation
handle_event({call, From}, {handle_message, Msg, SessId},
             wait_for_result, Data) ->
    MsgType = ct_msg:get_type(Msg),
    {NewState, NewData} = handle_message(MsgType, Msg, SessId, Data),
    {next_state, NewState, NewData, {reply, From, ok}};

handle_event({call, From}, {handle_message, Msg, SessId},
             cancelled, Data) ->
    MsgType = ct_msg:get_type(Msg),
    {NewState, NewData} = handle_cancelled_message(MsgType, Msg, SessId, Data),
    {next_state, NewState, NewData, {reply, From, ok}};

%% stop the invocation when done
handle_event(enter, _, done, Data) ->
    #data{id = Id, realm = Realm} = Data,
    Msg = "invocation ~p/~p is done",
    lager:debug(Msg, [Realm, Id]),
    ok = ctrd_invocation:delete(Realm, Id),
    {stop, normal, Data};


%% the timeout is reached
handle_event({timeout, auto_cancel}, _, wait_for_result,
             #data{caller_req_id = ReqId, caller_sess = CallerSess} = Data) ->
    %% have cancelled error message for now
    Error = ?ERROR(call, ReqId, #{}, cancelled),
    ct_router:to_session(CallerSess, Error),
    {next_state, cancelled, Data};

%% handle the session terminated event sent by the system
handle_event(cast, {session_terminated, SessId}, _State,
             #data{caller_sess = CallerSess} = Data) ->
    CallerSessId = cta_session:get_id(CallerSess),
    handle_session_termination(SessId, CallerSessId, Data);

%% ignore non specificed state enter
handle_event(enter, _, State, Data) ->
    {next_state, State, Data};

%% error on unkonwn / ignored events
handle_event(Event, Content, State, #data{id = Id} = Data) ->
    lager:error("[~p] {~p} ignore event ~p - ~p", [Id, State, Event, Content]),
    {next_state, State, Data}.



maybe_timeout_action(I) when is_integer(I), I > 0 ->
    [{{timeout, auto_cancel}, I, ignored}];
maybe_timeout_action(_)  ->
    [].


%% handle the yield message
handle_message(yield, Msg, SenderSessId, #data{caller_req_id = ReqId,
                                              %options=ReqOptions,
                                               callees_waiting_for = CalleeIds
                                              } = Data ) ->
    {yield, _Id, Options, Args, ArgsKw} = Msg,
    IsAllowed = lists:member(SenderSessId, CalleeIds),
    IsProgress = maps:get(progress, Options, false),
    %% ReqProgress = maps:get(progress, ReqOptions, false),

    %% if the message is not allowed: do not care
    %% true =  (not IsAllowed) or ( ReqProgress or (not IsProgress)),

    ResultMsg = result_msg(ReqId, IsProgress, Args, ArgsKw),
    handle_result_message(IsAllowed, IsProgress, ResultMsg, SenderSessId, Data);

%handle the error message
handle_message(error, Msg, SenderSessId, #data{callees_waiting_for = CalleeIds,
                                               caller_req_id = ReqId} = Data) ->
    {error, invocation, _ReqId, _Details, Error, Args, ArgsKw} = Msg,
    IsAllowed = lists:member(SenderSessId, CalleeIds),
    ResultMsg = ?ERROR(call, ReqId, #{}, Error, Args, ArgsKw),
    handle_result_message(IsAllowed, false, ResultMsg, SenderSessId, Data);


% handle the cancel message
handle_message(cancel, Msg, SenderSessId, #data{caller_req_id = ReqId,
                                               caller_sess = CallerSess,
                                               callees = CalleeIds} = Data) ->
    {cancel, ReqId, Options} = Msg,
    ReqMode = ct_utils:to_existing_atom(maps:get(mode, Options, skip)),
    Mode = get_supported_cancel_mode(ReqMode, CalleeIds),

    IsAllowed = (cta_session:get_id(CallerSess) == SenderSessId),
    maybe_cancel(IsAllowed, Mode, Data);


%% error on unsupported messages
handle_message(_, Msg, SenderSessId, Data) ->
     lager:error("received unknown / unsupported message '~p' by session ~p",
                 [Msg, SenderSessId]),
     {wait_for_result, Data}.



handle_result_message(false, _, Msg, SessId, Data) ->
    lager:warning("ignoring result message '~p' by session ~p", [Msg, SessId]),
    {wait_for_result, Data};
handle_result_message(true, IsProgress, ResultMsg, SenderSessId, Data) ->
    #data{ caller_sess = CallerSess} = Data,
    NewData = update_data_for_result_message(IsProgress, SenderSessId, Data),
    NewState = maybe_done(NewData, wait_for_result),
    ct_router:to_session(CallerSess, ResultMsg),
    {NewState, NewData}.


handle_cancelled_message(error, _, SenderSessId, Data) ->
    NewData = update_data_for_result_message(false, SenderSessId, Data),
    NewState = maybe_done(NewData, cancelled),
    {NewState, NewData};
handle_cancelled_message(yield, Msg, SenderSessId, Data) ->
    {yield, _Id, Options, _, _} = Msg,
    IsProgress = maps:get(progress, Options, false),
    NewData = update_data_for_result_message(IsProgress, SenderSessId, Data),
    NewState = maybe_done(NewData, cancelled),
    {NewState, NewData};
handle_cancelled_message(_, _Msg, _SenderSessId, Data) ->
    {cancelled, Data}.


update_data_for_result_message(true, _SessId, Data) ->
    Data;
update_data_for_result_message(false, SessId, Data) ->
    #data{ callees_waiting_for = Callees } = Data,
    NewCallees = lists:delete(SessId, Callees),
    Data#data{callees_waiting_for = NewCallees}.


maybe_done(#data{callees_waiting_for = []}, _State) ->
    done;
maybe_done(_, State) ->
    State.



%% handle termination of a session related to this invocation
handle_session_termination(CallerSessId, CallerSessId, Data) ->
    %% the caller terminated ... mark this invocation as done
    %% this will trigger the deletion of this invocation
    {next_state, done, Data};
handle_session_termination(SessId, _CallerSessId,
                           #data{caller_req_id = ReqId} = Data) ->
    %% a callee terminated
    %% just send a fake cancel message from the callee
    CancelMsg = ?ERROR(invocation, ReqId, #{}, canceled),
    NewState = handle_message(error, CancelMsg, SessId, Data),
    {next_state, NewState, Data}.


result_msg(ReqId, true, Args, ArgsKw) ->
    ?RESULT(ReqId, #{progress => true}, Args, ArgsKw);
result_msg(ReqId,  _, Args, ArgsKw) ->
    ?RESULT(ReqId, #{}, Args, ArgsKw).


maybe_cancel(true, skip, #data{caller_sess = CallerSess,
                               caller_req_id = ReqId} = Data) ->
    Error = ?ERROR(call, ReqId, #{}, cancelled),
    ct_router:to_session(CallerSess, Error),
    {cancelled, Data};
maybe_cancel(true, kill, #data{id = Id, callees = CalleeIds} = Data) ->
    Interrupt = ?INTERRUPT(Id, #{}),
    send_to_sessions(Interrupt, CalleeIds),
    {wait_for_result, Data};
maybe_cancel(true, killnowait, #data{caller_sess = CallerSess,
                                     caller_req_id = ReqId, id = Id,
                                     callees = CalleeIds} = Data) ->
    Error = ?ERROR(call, ReqId, #{}, cancelled),
    ct_router:to_session(CallerSess, Error),
    Interrupt = ?INTERRUPT(Id, #{}),
    send_to_sessions(Interrupt, CalleeIds),
    {cancelled, Data};
maybe_cancel(false, _Mode, Data) ->
    % ignore the message from unknown caller
    {wait_for_result, Data}.

match_registration(true, _, _) ->
    {ok, system};
match_registration(false, Procedure, Session) ->
    Realm = cta_session:get_realm(Session),
    ctr_registration:match(Procedure, Realm).



send_invocation(InvocId, RegistrationId, CalleeIds, Args, ArgsKw, Data) ->
    #data{options = Options} = Data,
    Details = gen_invocation_details(maps:to_list(Options), #{}, Data),
    InvocMsg = ?INVOCATION(InvocId, RegistrationId, Details, Args, ArgsKw),
    send_to_sessions(InvocMsg, CalleeIds).


send_to_sessions(Msg, SessionIds) ->
    ToSession =
        fun(Id, Sessions) ->
            case cta_session:lookup_by_id(Id) of
                {ok, Session} -> [ Session | Sessions ];
            _ -> Sessions
        end
    end,
    Sessions = lists:foldl(ToSession, [], SessionIds),
    Send = fun(Session, _) ->
               ct_router:to_session(Session, Msg)
           end,
    lists:foldl(Send, ok, Sessions),
    ok.


gen_invocation_details([], Details, _Data) ->
    Details;
gen_invocation_details([{disclose, true} | T ], Details,
                       #data{caller_sess = Session} = Data) ->
    SessionId = cta_session:get_id(Session),
    NewDetails = maps:put(caller, SessionId, Details),
    gen_invocation_details(T, NewDetails, Data);
gen_invocation_details([{progress, true} | T ], Details, Data) ->
    NewDetails = maps:put(receive_progress, true, Details),
    gen_invocation_details(T, NewDetails, Data);
gen_invocation_details([ _ | T ], Details, Data) ->
    gen_invocation_details(T, Details, Data).


get_supported_cancel_mode(ReqMode, CalleeIds)
  when ReqMode == skip; ReqMode == kill; ReqMode == killnowait ->
    CheckSupport =
      fun(CalleeId, Mode) ->
        {ok, Session} = cta_session:lookup_by_id(CalleeId),
        Details = cta_session:get_details(Session),
        Roles = maps:get(roles, Details, #{}),
        Callee = maps:get(callee, Roles, #{}),
        Features = maps:get(features, Callee, #{}),
        CallCanceling = maps:get(call_canceling, Features, false),
        case CallCanceling of
          true -> Mode;
          _ -> skip
        end
      end,
    lists:foldl(CheckSupport, ReqMode, CalleeIds).



terminate(normal, done, _Data) ->
    ok;
terminate(normal, _State, Data) ->
    delete_table_entries(Data);
terminate(Reason, wait_for_result = State, Data) ->
    #data{caller_sess = CallerSess, caller_req_id = ReqId} = Data,
    Error = ?ERROR(call, ReqId, #{}, <<"org.cargotube.error.internal">>),
    ct_router:to_session(CallerSess, Error),
    log_termination(Reason, State, Data);
terminate(Reason, State, Data) ->
    log_termination(Reason, State, Data).


delete_table_entries(Data) ->
    #data{id = Id, realm = Realm} = Data,
    ctrd_invocation:delete(Realm, Id),
    ok.

log_termination(Reason, State, Data) ->
    #data{id = Id, realm = Realm} = Data,
    Msg = "invocation~p / ~p terminated in state ~p with reason ~p",
    lager:error(Msg, [Realm, Id, State, Reason]),
    delete_table_entries(Data).



code_change(_OldVsn, State, Data, _Extra) ->
    {ok, State, Data}.


