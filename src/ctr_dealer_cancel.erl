-module(ctr_dealer_cancel).

-include("ctr_data.hrl").
-include_lib("ct_msg/include/ct_msg.hrl").

-export([
         handle/2,
         cancel_all/1
        ]).


handle(Message, Session) ->
    Realm = cta_session:get_realm(Session),
    {ok, ReqId} = ct_msg:get_request_id(Message),
    SessId = cta_session:get_id(Session),
    MaybeInvoc = ctrd_invocation:get_invocation_by_caller(Realm, SessId, ReqId),
    ok = handle_maybe_invocation(MaybeInvoc, Message, Session).


handle_maybe_invocation({ok, Invocation}, Msg, Session) ->
    SessId = cta_session:get_id(Session),
    Pid = ctrd_invocation:get_pid(Invocation),
    ctr_invocation:handle_message(Msg, SessId, Pid);
handle_maybe_invocation(_Error, Session, Msg) ->
    {ok, ReqId} = ct_msg:get_request_id(Msg),
    MsgType = ct_msg:get_type(Msg),
    ok = ct_router:to_session(Session, ?ERROR(MsgType, ReqId, #{},
                                              no_such_invocation)),
    ok.

cancel_all(_Session) ->
    ok.

