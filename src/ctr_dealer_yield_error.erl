-module(ctr_dealer_yield_error).

-include("ctr_data.hrl").
-include_lib("ct_msg/include/ct_msg.hrl").

-export([
        handle/2
        ]).

handle(Message, Session) ->
    {ok, InvocId} = ct_msg:get_request_id(Message),
    MaybeInvocation = ctrd_invocation:get_invocation_callees(InvocId),
    ok = handle_maybe_invocation(MaybeInvocation, Message, Session).

handle_maybe_invocation([Invocation | _ ], Msg, Session) ->
    SessionId = cta_session:get_id(Session),
    Pid = ctrd_invocation:get_pid(Invocation),
    ctr_invocation:handle_message(Msg, SessionId, Pid);
handle_maybe_invocation(_, Msg, Session) ->
    Realm = cta_session:get_realm(Session),
    {ok, InvocId} = ct_msg:get_request_id(Msg),
    MsgType = ct_msg:get_type(Msg),
    Info = "dropping invocation result: msg type ~p for "
           "invocation ~p / ~p due to not found",
    lager:info(Info, [MsgType, Realm, InvocId]),
    ok.
