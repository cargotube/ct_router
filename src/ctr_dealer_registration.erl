-module(ctr_dealer_registration).

-include("ctr_data.hrl").
-include_lib("ct_msg/include/ct_msg.hrl").

-export([
         do_register/2,
         do_unregister/2,
         unregister_all/1
        ]).

do_register({register, _ReqId, _Options, Procedure} = Msg, Session) ->
    Result = ctr_registration:add(Procedure, exact, Session),
    handle_register_result(Result, Msg, Session).


do_unregister({unregister, _ReqId, RegId} = Msg, Session) ->
    Result = ctr_registration:remove(RegId, Session),
    handle_unregister_result(Result, Msg, Session).

unregister_all(Session) ->
    %% unregister all registrations
    Regs = cta_session:get_registrations(Session),
    Delete = fun(RegId, ok) ->
                     ctr_registration:remove(RegId, Session),
                     ok
             end,
    lists:foldl(Delete, ok, Regs),

    %% send session termination to related invocations
    SessId = cta_session:get_id(Session),
    CallerInvocs = ctrd_invocation:get_invocations_of_caller(SessId),
    CalleeInvocs = ctrd_invocation:get_invocations_of_callee(SessId),
    Invocs = CallerInvocs ++ CalleeInvocs,
    Cancel = fun(Invoc, ok) ->
                   Pid = ctrd_invocation:get_pid(Invoc),
                   ctr_invocation:session_terminated(SessId, Pid)
             end,
    lists:foldl(Cancel, ok, Invocs),
    ok.


handle_register_result({created, Registration}, Msg, Session) ->
    ctr_broker:send_registration_meta_event(create, Session, Registration),
    send_registered(Msg, Registration, Session);
handle_register_result({error, procedure_exists}, Msg, Session) ->
    {ok, ReqId} = ct_msg:get_request_id(Msg),
    ok = ct_router:to_session(Session, ?ERROR(register, ReqId, #{},
                                              procedure_already_exists)),
    ok.

handle_unregister_result({atomic, {removed, Registration}}, Msg, Session) ->
    send_unregistered(Msg, Registration, Session);
handle_unregister_result({atomic, {deleted, Registration}}, Msg, Session) ->
    send_unregistered(Msg, Registration, Session),
    ctr_broker:send_registration_meta_event(delete, Session, Registration),
    ok;
handle_unregister_result({atomic, {error, not_found}}, Msg, Session) ->
    {unregister, _, RegId} = Msg,
    false = cta_session:has_registration(RegId, Session),
    {ok, RequestId} = ct_msg:get_request_id(Msg),
    Error = ?ERROR(unregister, RequestId, #{}, no_such_registration),
    ok = ct_router:to_session(Session, Error),
    ok.

send_registered(Msg, Registration, Session) ->
    ctr_broker:send_registration_meta_event(register, Session, Registration),
    RegId = ctr_registration:get_id(Registration),
    {ok, NewSession} = cta_session:add_registration(RegId, Session),
    {ok, RequestId} = ct_msg:get_request_id(Msg),
    ok = ct_router:to_session(NewSession, ?REGISTERED(RequestId, RegId)),
    ok.

send_unregistered(Msg, Registration, Session) ->
    ctr_broker:send_registration_meta_event(unregister, Session, Registration),
    RegId = ctr_registration:get_id(Registration),
    {ok, NewSession} = cta_session:remove_registration(RegId, Session),
    {ok, RequestId} = ct_msg:get_request_id(Msg),
    ok = ct_router:to_session(NewSession, ?UNREGISTERED(RequestId)),
    ok.

