-module(ctr_dealer).

-include("ctr_data.hrl").
-include_lib("ct_msg/include/ct_msg.hrl").

-export([
         handle_message/3,
         unregister_all/1
        ]).

handle_message(register, Message, Session) ->
    ctr_dealer_registration:do_register(Message, Session);
handle_message(unregister, Message, Session) ->
    ctr_dealer_registration:do_unregister(Message, Session);
handle_message(call, Message, Session) ->
    do_call(Message, Session);

%% messages handled by the invocation process
handle_message(cancel, Message, Session) ->
    ctr_dealer_cancel:handle(Message, Session);
handle_message(error, Message, Session) ->
    ctr_dealer_yield_error:handle(Message, Session);
handle_message(yield, Message, Session) ->
    ctr_dealer_yield_error:handle(Message, Session).


unregister_all(Session) ->
    ctr_dealer_registration:unregister_all(Session).

do_call(Msg, Session) ->
    {ok, Pid} =  ctr_invocation:new(Msg, Session),
    %% a blocking call to the invocation so the ordering is guaranteed
    ok = ctr_invocation:send_invocation(Pid).

