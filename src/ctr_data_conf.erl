-module(ctr_data_conf).

-define(MNESIADIR, <<"db/mnesia">>).

-record(ctr_data_config,
    {
        data_sub_mod = ctr_data_off,
        data_pub_mod = ctr_data_off,
        data_reg_mod = ctr_data_off,
        data_inv_mod = ctr_data_off,

        max_keep = 60 * 60 * 6,
        clean_interval = 500,

        mnesia_dir = ?MNESIADIR,

        config = #{}
    }).

-export([new/1,

         raw_conf/1,
         inv_mod/1,
         pub_mod/1,
         reg_mod/1,
         sub_mod/1,
         clean_config/1,

         mnesia_dir/1
        ]).


new(Config) when is_map(Config) ->
    InvMod = get_atom_value(inv_mod, Config, ctr_data_ram_invocation),
    PubMod = get_atom_value(pub_mod, Config, ctr_data_ram_publication),
    RegMod = get_atom_value(reg_mod, Config, ctr_data_ram_registration),
    SubMod = get_atom_value(sub_mod, Config, ctr_data_ram_subscription),

    CleanInterval = maps:get(clean_interval, Config, 300),
    MaxKeep = maps:get(max_keep, Config, 60 * 60 * 6),

    MnesiaDir= maps:get(mnesia_dir, Config, ?MNESIADIR),

    #ctr_data_config{data_inv_mod = InvMod,
                     data_pub_mod = PubMod,
                     data_reg_mod = RegMod,
                     data_sub_mod = SubMod,

                     max_keep = MaxKeep,
                     clean_interval = CleanInterval,
                     mnesia_dir = MnesiaDir,

                     config = Config}.

raw_conf(#ctr_data_config{config = Config}) ->
   Config.

inv_mod(#ctr_data_config{data_inv_mod = Module}) ->
   Module.

pub_mod(#ctr_data_config{data_pub_mod = Module}) ->
   Module.

reg_mod(#ctr_data_config{data_reg_mod = Module}) ->
   Module.

sub_mod(#ctr_data_config{data_sub_mod = Module}) ->
   Module.

mnesia_dir(#ctr_data_config{mnesia_dir = MnesiaDir}) ->
    MnesiaDir.

clean_config(#ctr_data_config{
                max_keep = MaxKeep,
                clean_interval = CleanInterval
                }) ->
    #{max_age => MaxKeep, interval => CleanInterval}.


get_atom_value(Key, Map, Default) when is_atom(Default) ->
    Value = maps:get(Key, Map, Default),
    MaybeAtom = maybe_to_atom(Value),
    return_value(MaybeAtom, Default).

maybe_to_atom(Value) when is_binary(Value) ->
    try
        binary_to_existing_atom(Value, utf8)
     catch
         _ -> Value
     end;
maybe_to_atom(Atom) when is_atom(Atom) ->
    Atom.

return_value(Atom, _) when is_atom(Atom) ->
    Atom;
return_value(_, Default) ->
    Default.
