-module(ctr_data).

-export([
         list_subscriptions/1,
         lookup_subscription/3,
         match_subscription/2,
         get_subscription/2,
         add_subscription/4,
         remove_subscription/3,

         store_publication/1,
         clean_publications/1,

         list_registrations/1,
         lookup_registration/3,
         match_registration/2,
         get_registration/2,
         add_registration/4,
         remove_registration/3,

         add_invocation/1,
         add_invocation_callee/1,
         invocation_add_result/3,
         invocation_set_state/3,
         get_invocation_callees/1,
         get_invocations_of_caller/1,
         get_invocations_of_callee/1,
         get_invocation_by_caller/3,
         remove_invocation/2,

         init/1
        ]).

-define(SUB_RUN(Func, Args),
        {ok, DataConf} = application:get_env(cargotube, ?MODULE),
        ctr_data_subscription_if:do_run(Func, Args, DataConf)).
-define(PUB_RUN(Func, Args),
        {ok, DataConf} = application:get_env(cargotube, ?MODULE),
        ctr_data_publication_if:do_run(Func, Args, DataConf)).
-define(REG_RUN(Func, Args),
        {ok, DataConf} = application:get_env(cargotube, ?MODULE),
        ctr_data_registration_if:do_run(Func, Args, DataConf)).
-define(INV_RUN(Func, Args),
        {ok, DataConf} = application:get_env(cargotube, ?MODULE),
        ctr_data_invocation_if:do_run(Func, Args, DataConf)).

list_subscriptions(Realm) ->
    ?SUB_RUN(?FUNCTION_NAME, [Realm]).

lookup_subscription(Procedure, Options, Realm) ->
    ?SUB_RUN(?FUNCTION_NAME, [Procedure, Options, Realm]).


match_subscription(Procedure, Realm) ->
    ?SUB_RUN(?FUNCTION_NAME, [Procedure, Realm]).


get_subscription(ProcedureId, Realm) ->
    ?SUB_RUN(?FUNCTION_NAME, [ProcedureId, Realm]).


add_subscription(Uri, Match, SessionId, Realm) ->
    ?SUB_RUN(?FUNCTION_NAME, [Uri, Match, SessionId, Realm]).


remove_subscription(SubscriptionId, SessionId, Realm) ->
    ?SUB_RUN(?FUNCTION_NAME, [SubscriptionId, SessionId, Realm]).


store_publication(Publication) ->
    ?PUB_RUN(?FUNCTION_NAME, [Publication]).

clean_publications(MaxAge) ->
    ?PUB_RUN(?FUNCTION_NAME, [MaxAge]).


list_registrations(Realm) ->
    ?REG_RUN(?FUNCTION_NAME, [Realm]).


lookup_registration(Procedure, Options, Realm) ->
    ?REG_RUN(?FUNCTION_NAME, [Procedure, Options, Realm]).


match_registration(Procedure, Realm) ->
    ?REG_RUN(?FUNCTION_NAME, [Procedure, Realm]).

get_registration(ProcedureId, Realm) ->
    ?REG_RUN(?FUNCTION_NAME, [ProcedureId, Realm]).


add_registration(Procedure, Match, SessionId, Realm) ->
    ?REG_RUN(?FUNCTION_NAME, [Procedure, Match, SessionId, Realm]).

remove_registration(RegistrationId, SessionId, Realm) ->
    ?REG_RUN(?FUNCTION_NAME, [RegistrationId, SessionId, Realm]).

add_invocation(Invocation) ->
    ?INV_RUN(?FUNCTION_NAME, [Invocation]).

add_invocation_callee(Invocation) ->
    ?INV_RUN(?FUNCTION_NAME, [Invocation]).

invocation_set_state(State, InvocationId, Realm) ->
    ?INV_RUN(?FUNCTION_NAME, [State, InvocationId, Realm]).

invocation_add_result(Result, InvocationId, Realm) ->
    ?INV_RUN(?FUNCTION_NAME, [Result, InvocationId, Realm]).

get_invocation_callees(InvocationId) ->
    ?INV_RUN(?FUNCTION_NAME, [InvocationId]).

get_invocation_by_caller(Realm, SessionId, ReqId) ->
    ?INV_RUN(?FUNCTION_NAME, [Realm, SessionId, ReqId]).

get_invocations_of_caller(SessId) ->
    ?INV_RUN(?FUNCTION_NAME, [SessId]).

get_invocations_of_callee(SessId) ->
    ?INV_RUN(?FUNCTION_NAME, [SessId]).

remove_invocation(Realm, InvocationId) ->
    ?INV_RUN(?FUNCTION_NAME, [Realm, InvocationId]).

init(Config) ->
    DataConf = ctr_data_conf:new(Config),
    ctr_data_subscription_if:initialize(DataConf),
    ctr_data_publication_if:initialize(DataConf),
    ctr_data_registration_if:initialize(DataConf),
    ctr_data_invocation_if:initialize(DataConf),
    CleaningConfig = ctr_data_conf:clean_config(DataConf),
    ctr_data_cleaner:start(CleaningConfig),
    application:set_env(cargotube, ?MODULE, DataConf),
    {ok, DataConf}.
