-module(ctr_invocation_sup).
-behaviour(supervisor).

-export([new_invocation/1]).

-export([start_link/0]).
-export([init/1]).

-spec start_link() -> {ok, pid()}.
start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, noparams).

init(noparams) ->
    Procs = [
                invocation_worker()
            ],

    Flags = #{strategy => simple_one_for_one },
    {ok, {Flags, Procs}}.


invocation_worker() ->
    #{ id => invocation,
       restart => temporary,
       start => {ctr_invocation, start_link, []}
     }.

new_invocation(ParamMap) when is_map(ParamMap) ->
    supervisor:start_child(?MODULE, [ParamMap]).
