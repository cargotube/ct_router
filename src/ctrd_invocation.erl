-module(ctrd_invocation).


-include_lib("ct_msg/include/ct_msg.hrl").
-include("ctr_data.hrl").

-export([
         new/3,
         add_callees/3,

         get_pid/1,

         get_invocation_callees/1,
         get_invocations_of_callee/1,
         get_invocations_of_caller/1,
         get_invocations_by_caller/2,
         get_invocation_by_caller/3,
         delete/2

        ]).

new(Pid,  CallerReqId, CallerSession) ->
    Realm = cta_session:get_realm(CallerSession),
    CallerSessId = cta_session:get_id(CallerSession),
    Invoc = #ctr_invocation_caller{
               id = undefined,
               ts = undefined,
               sess_id = CallerSessId,
               realm = Realm,
               req_id = CallerReqId,
               pid = Pid
              },
    store_invocation(Invoc).

add_callees(Pid, InvocId, CalleeSessIds) ->
    Add = fun(SessId, _) ->
        Invoc = #ctr_invocation_callee{
                   id = InvocId,
                   sess_id = SessId,
                   pid = Pid
                  },
        store_invocation_callee(Invoc)
    end,
    ok =lists:foldl(Add, ok, CalleeSessIds),
    ok.

get_pid(#ctr_invocation_caller{pid = Pid}) ->
    Pid;
get_pid(#ctr_invocation_callee{pid = Pid}) ->
    Pid.

store_invocation(Invoc) ->
    ctr_data:add_invocation(Invoc).

store_invocation_callee(Invoc) ->
    ctr_data:add_invocation_callee(Invoc).


get_invocation_callees(InvocId) ->
    ctr_data:get_invocation_callees(InvocId).

get_invocations_of_caller(SessId) ->
    ctr_data:get_invocations_of_caller(SessId).

get_invocations_of_callee(SessId) ->
    ctr_data:get_invocations_of_callee(SessId).

get_invocations_by_caller(Realm, SessId) ->
    ctr_data:get_invocations_by_caller(SessId, Realm).

get_invocation_by_caller(Realm, SessId, ReqId) ->
    ctr_data:get_invocation_by_caller(SessId, ReqId, Realm).

delete(Realm, InvocationId) ->
    ctr_data:remove_invocation(Realm, InvocationId).
