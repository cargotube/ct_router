-module(ctr_data_ram_invocation).
-behaviour(ctr_data_invocation_if).

-include("ctr_data.hrl").

-export([
         add_invocation/1,
         add_invocation_callee/1,
         get_invocation_callees/1,
         get_invocations_of_callee/1,
         get_invocations_of_caller/1,
         get_invocation_by_caller/3,
         remove_invocation/2,

         init/1,
         dump/0
        ]).


init(DataConf) ->
    create_table(DataConf).


add_invocation(#ctr_invocation_caller{} = Invoc) ->
    InvocId = ct_utils:gen_global_id(),
    Now = erlang:system_time(second),
    NewInvocCaller = Invoc#ctr_invocation_caller{id = InvocId, ts = Now},

    StoreInvocation =
        fun() ->
                case mnesia:wread({ctr_invocation_caller, InvocId}) of
                    [] ->
                        ok = mnesia:write(NewInvocCaller),
                        {ok, NewInvocCaller};
                    _ ->
                        {error, id_exists}
                end
        end,
    Result = mnesia:transaction(StoreInvocation),
    handle_invocation_store_result(Result, Invoc).

handle_invocation_store_result({atomic, {ok, Invoc}}, _) ->
    {ok, Invoc};
handle_invocation_store_result({atomic, {error, id_exists}},
                               #ctr_invocation_callee{}) ->
    {error, id_exists};
handle_invocation_store_result({atomic, {error, id_exists}}, Invoc) ->
    add_invocation(Invoc).

add_invocation_callee(#ctr_invocation_callee{} = InvocCallee) ->
    StoreInvocation =
        fun() ->
            ok = mnesia:write(InvocCallee)
        end,
    Result = mnesia:transaction(StoreInvocation),
    simplify_result(Result).



get_invocation_callees(InvocId) ->
    FindInvocation =
        fun() ->
                case mnesia:read({ctr_invocation_callee, InvocId}) of
                    Invocs when is_list(Invocs) -> Invocs;
                    _ -> []
                end
        end,
    Result = mnesia:transaction(FindInvocation),
    simplify_result(Result).

get_invocations_of_callee(SessionId) ->
    FindInvocation =
        fun() ->
                Pos = #ctr_invocation_callee.sess_id,
                case mnesia:index_read(ctr_invocation_callee, SessionId, Pos) of
                    Invocs when is_list(Invocs) -> Invocs;
                    _ -> []
                end
        end,
    Result = mnesia:transaction(FindInvocation),
    simplify_result(Result).

get_invocations_of_caller(SessionId) ->
    FindInvocation =
        fun() ->
                Pos = #ctr_invocation_caller.sess_id,
                case mnesia:index_read(ctr_invocation_caller, SessionId, Pos) of
                    Invocs when is_list(Invocs) -> Invocs;
                    _ -> []
                end
        end,
    Result = mnesia:transaction(FindInvocation),
    simplify_result(Result).

get_invocation_by_caller(Realm, SessId, ReqId) ->
    FindInvocation =
        fun() ->
                case mnesia:read(ctr_invocation_caller,
                                 {Realm, SessId, ReqId}) of
                    [Invoc] -> {ok, Invoc};
                    _ -> {error, not_found}
                end
        end,
    Result = mnesia:transaction(FindInvocation),
    handle_invocation_find_result(Result).

handle_invocation_find_result({atomic, {ok, Invocation}}) ->
    {ok, Invocation};
handle_invocation_find_result(_) ->
    {error, not_found}.



remove_invocation(_Realm, InvocId) ->
    Delete = fun() ->
                case mnesia:wread({ctr_invocation_caller, InvocId}) of
                    [_] ->
                        ok = mnesia:delete({ctr_invocation_callee, InvocId}),
                        ok = mnesia:delete({ctr_invocation_caller, InvocId}),
                        ok;
                    _ -> {error, not_found}
                end
             end,
    Result = mnesia:transaction(Delete),
    simplify_result(Result).

simplify_result({atomic, ok}) -> ok; simplify_result({atomic, Error}) ->
    Error.



dump() ->
    Dump = fun(Record, ok) ->
               lager:debug("~p", [Record]),
               ok
           end,
    Transaction = fun() ->
                      lager:debug("****** caller *****"),
                      mnesia:foldl(Dump, ok, ctr_invocation_caller),
                      lager:debug("****** callee *****"),
                      mnesia:foldl(Dump, ok, ctr_invocation_callee),
                      lager:debug("****** end *****"),
                      ok
                  end,
    mnesia:transaction(Transaction).


create_table(DataConf) ->
    MnesiaDir = ctr_data_conf:mnesia_dir(DataConf),
    ct_data_util:create_mnesia_schema_if_needed(MnesiaDir),
    mnesia:delete_table(ctr_invocation_caller),
    InvDef1 = [{attributes, record_info(fields, ctr_invocation_caller)},
              {ram_copies, [node()]},
              {index, [sess_id, ts]}
             ],
    {atomic, ok} = mnesia:create_table(ctr_invocation_caller, InvDef1),

    mnesia:delete_table(ctr_invocation_callee),
    InvDef2 = [{attributes, record_info(fields, ctr_invocation_callee)},
              {ram_copies, [node()]},
              {index, [sess_id]},
              {type, bag}
             ],
    {atomic, ok} = mnesia:create_table(ctr_invocation_callee, InvDef2),
    ok.
