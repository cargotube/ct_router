-module(ctr_data_invocation_if).
-include("ctr_data.hrl").

-export([
         initialize/1,
         do_run/3
        ]).

-callback init(DataConf :: term()) -> ok.


-type uri() :: binary().
-type error_reason() :: any().
-type id() :: non_neg_integer().
-type invocation_caller() :: #ctr_invocation_caller{}.
-type invocation_callee() :: #ctr_invocation_callee{}.
-type invocation() :: invocation_caller() | invocation_callee().


-callback add_invocation( Invocation :: invocation_caller() ) ->
    {ok, UpdatedInvocation :: invocation_caller()} | {error, Reason :: any()}.

-callback add_invocation_callee( Invocation :: invocation_callee() ) ->
    {ok} | {error, Reason :: any()}.

-callback get_invocation_callees(InvocationId :: id()) ->
    [Invocation :: invocation_callee()].

-callback get_invocations_of_caller(SessionId :: id()) ->
    [Invocation :: invocation_caller()].

-callback get_invocations_of_callee(SessionId :: id()) ->
    [Invocation :: invocation_callee()].

-callback get_invocation_by_caller(Realm :: uri(), CallerSessId :: id(),
                                   ReqId :: id()) ->
    {ok, Invocation :: invocation()} | {error, Reason :: error_reason()}.

-callback remove_invocation(InvocationId :: id(), Realm :: uri()) ->
    ok | {error, Reason :: error_reason()}.


initialize(DataConf) ->
    Module = ctr_data_conf:inv_mod(DataConf),
    lager:info("data invocation interface is ~p", [Module]),
    Module:init(DataConf).

do_run(Function, Arguments, DataConf) ->
    Module = ctr_data_conf:inv_mod(DataConf),
    apply(Module, Function, Arguments).

