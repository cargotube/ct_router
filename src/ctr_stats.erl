-module(ctr_stats).

-export([
         init/1,
         add/2,
         update/0,
         enabled/0]).

init(_Config) ->
    {ok, Pid} = ct_stats:new(#{name => <<"router">>}),
    application:set_env(ct_router, enable_stats, Pid).

add(Type, Duration) ->
    maybe_add(enabled(), Type, Duration).

maybe_add(Pid, Type, Duration) when is_pid(Pid) ->
    ct_stats:add_message(Type, Duration/1000.00, Pid);
maybe_add(_, _Trype, _Duration) ->
    ok.

enabled() ->
    application:get_env(ct_router, enable_stats, undefined).

update() ->
    maybe_update(enabled()).

maybe_update(Pid) when is_pid(Pid) ->
    ct_stats:update(Pid);
maybe_update(_) ->
    ok.
